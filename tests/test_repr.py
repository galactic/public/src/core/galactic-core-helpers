"""Repr test module."""

from unittest import TestCase

from galactic.helpers.core import default_repr


class ReprTest(TestCase):
    def test_default_repr(self):
        class Dummy: ...

        dummy = Dummy()
        self.assertEqual(
            default_repr(dummy),
            f"<tests.test_repr.Dummy object at 0x{id(dummy):x}>",
        )
        self.assertEqual(
            default_repr(dummy, class_name="MyClass"),
            f"<tests.test_repr.MyClass object at 0x{id(dummy):x}>",
        )
        self.assertEqual(
            default_repr(dummy, long=False),
            f"<Dummy object at 0x{id(dummy):x}>",
        )
        self.assertEqual(
            default_repr(dummy, class_name="MyClass", long=False),
            f"<MyClass object at 0x{id(dummy):x}>",
        )
