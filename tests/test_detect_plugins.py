"""Test module."""

import subprocess
import sys
from io import StringIO
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

from galactic.helpers.core import detect_plugins


class PluginsTestCase(TestCase):
    def setUp(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "install",
                Path(__file__).parent / "package",
            ],
        )

    @patch("sys.stdout", new_callable=StringIO)
    def test_package(self, stdout: StringIO):
        detect_plugins()
        # Check stdout
        self.assertEqual(stdout.getvalue(), "package is installed\n")

    def tearDown(self):
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "uninstall",
                "-y",
                "package",
            ],
        )
