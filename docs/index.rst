=========================
**GALACTIC** helpers core
=========================

*galactic-helpers-core* [#logo]_ [#logohelperscore]_
is the library implementing core helpers.

**GALACTIC** stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Reference

    api/helpers/core/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logohelperscore]
        The *galactic-helpers-core* icon has been constructed using the main
        **GALACTIC** icon, the
        `Donor, helper, contributor icon <https://www.iconfinder.com/icons/10559916/donor_helper_contributor_supporter_sponsor_icon>`_
        designed by
        `Vectors Tank <https://www.iconfinder.com/vectorstank>`_
        and the
        `Heart icon <https://www.iconfinder.com/icons/111093/heart_icon>`_ designed by
        `WPZOOM <https://www.iconfinder.com/iconsets/wpzoom-developer-icon-set>`_.
