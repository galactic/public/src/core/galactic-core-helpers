"""
The :mod:`galactic.helpers.core` module.

It defines a function for detecting plugins and a function
for unifying string representation of objects.

* :func:`detect_plugins`
* :func:`default_repr`
"""

from ._plugins import detect_plugins
from ._repr import default_repr

__all__ = ("default_repr", "detect_plugins")
