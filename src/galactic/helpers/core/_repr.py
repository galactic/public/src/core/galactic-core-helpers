"""Module for representing objects."""

import itertools


def default_repr(
    item: object,
    class_name: str | None = None,
    long: bool = True,
) -> str:
    """
    Compute a string representation of the item.

    This function is designed to generate a string representation of a given Python
    object. This can be particularly useful for debugging or logging purposes,
    where a clear and informative representation of an object is needed.

    The function takes an object, an optional class name as input and a long flag.
    If the class name is provided, it is used as the class name in the representation.
    If long is True, the function constructs a string representation by combining
    the non-private elements of the module path (those not starting with an underscore)
    and the class name.

    Parameters
    ----------
    item
        A python object
    class_name
        An optional class name.
    long
        Does it use a long form (with the package name)?

    Returns
    -------
    str
        A string representation of the object

    """
    if class_name is None:
        class_name = item.__class__.__name__
    if long:
        class_name = ".".join(
            itertools.chain(
                (
                    element
                    for element in item.__class__.__module__.split(".")
                    if element[0] != "_"
                ),
                [class_name],
            ),
        )
    return f"<{class_name} object at 0x{id(item):x}>"
