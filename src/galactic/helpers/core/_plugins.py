"""plugin helper module."""

from __future__ import annotations

from importlib.metadata import entry_points


def detect_plugins(group: str = "galactic", name: str | None = None) -> None:
    """
    Detect plugins.

    It detect plugins declared by their group and execute their entry point as a
    function.

    Parameters
    ----------
    group
        The plugin group
    name
        The plugin name

    """
    for entry in entry_points(group=group):
        if name is None or name == entry.name:
            entry.load()()
